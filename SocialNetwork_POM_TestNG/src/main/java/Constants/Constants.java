package Constants;

public class Constants {

    public static final String BASE_URL = "http://localhost:8080";

    public static final String ALERT_EMPTY_REGISTRATION_FIELDS = "Please fill out this filed";
    public static final String ALERT_EMPTY_REGISTRATION_FIELDS_BG = "Моля, попълнете полето.";
    public static final String ERROR_WRONG_USERNAME_PASSWORD = "Wrong username or password.";

    public static final String UPDATED_FIRST_NAME = "First_Name_Updated";
    public static final String UPDATED_LAST_NAME = "Last_Name_Updated";
    public static final String UPDATED_TOWN_OF_BIRTH = "Town_Of_Birth_Updated";
    public static final String UPDATED_COUNTRY_OF_BIRTH = "Country_Of_Birth_Updated";
    public static final String UPDATED_TOWN_OF_RESIDENCE = "Town_Of_Residence_Updated";
    public static final String UPDATED_COUNTRY_OF_RESIDENCE = "Country_Of_Residence_Updated";
    public static final String GENDER_OTHER = "OTHER";
    public static final String PRIVACY_PRIVATE = "PRIVATE";
    public static final String USER_PICTURE = "\\src\\test\\resources\\UploadFiles\\tree.jpg";

    public static final String POST_EXAMPLE = "This is post example";
    public static final String COMMENT_EXAMPLE = "This is comment example";

    //DATA RELEVANT FOR ALL USERS
    public static final String ALL_USERS_TOWN_OF_BIRTH = "Sofia";
    public static final String ALL_USERS_COUNTRY_OF_BIRTH = "Bulgaria";
    public static final String ALL_USERS_TOWN_OF_RESIDENCE = "Sofia";
    public static final String ALL_USERS_COUNTRY_OF_RESIDENCE = "Bulgaria";
    public static final String ALL_USERS_VALID_PASSWORD = "A123456";
    public static final String EMPTY_STRING = "";

    //USER A DATA
    public static final String USER_A_USERNAME = "UserUI_A";
    public static final String USER_A_FIRST_NAME = "UI_A";
    public static final String USER_A_LAST_NAME = "UserUI_A";
    public static final String USER_A_EMAIL = "UserUI_A@abv.bg";
    public static final String USER_A_UPDATED_EMAIL = "Update_UI_A@abv.bg";

    //USER B DATA
    public static final String USER_B_USERNAME = "UserUI_B";
    public static final String USER_B_FIRST_NAME = "UI_B";
    public static final String USER_B_LAST_NAME = "UserUI_B";
    public static final String USER_B_EMAIL = "UserUI_B@abv.bg";

    //USER C DATA
    public static final String USER_C_USERNAME = "UserUI_C";
    public static final String USER_C_FIRST_NAME = "UI_C";
    public static final String USER_C_LAST_NAME = "UserUI_C";
    public static final String USER_C_EMAIL = "UserUI_C@abv.bg";


    //USER D DATA
    public static final String USER_D_USERNAME = "UserUI_D";
    public static final String USER_D_FIRST_NAME = "UI_D";
    public static final String USER_D_LAST_NAME = "UserUI_D";
    public static final String USER_D_EMAIL = "UserUI_D@abv.bg";
    public static final String USER_D_PUBLIC_POST = "Public post of UserUI_D";
    public static final String USER_D_PRIVATE_POST = "Private post of UserUI_D";
    public static final String USER_D_BUDDIES_ONLY_POST = "Buddies only post of UserUI_D";

    //USER E DATA
    public static final String USER_E_USERNAME = "UserUI_E";
    public static final String USER_E_FIRST_NAME = "UI_E";
    public static final String USER_E_LAST_NAME = "UserUI_E";
    public static final String USER_E_EMAIL = "UserUI_E@abv.bg";
    public static final String USER_E_PUBLIC_POST = "Public post of UserUI_E";
    public static final String USER_E_PRIVATE_POST = "Private post of UserUI_E";
    public static final String USER_E_BUDDIES_ONLY_POST = "Buddies only post of UserUI_E";

    //USER F DATA
    public static final String USER_F_USERNAME = "UserUI_F";
    public static final String USER_F_FIRST_NAME = "UI_F";
    public static final String USER_F_LAST_NAME = "UserUI_F";
    public static final String USER_F_EMAIL = "UserUI_F@abv.bg";
    public static final String USER_F_PUBLIC_POST = "Public post of UserUI_F";
    public static final String USER_F_PRIVATE_POST = "Private post of UserUI_F";
    public static final String USER_F_BUDDIES_ONLY_POST = "Buddies only post of UserUI_F";

    //USER G DATA
    public static final String USER_G_USERNAME = "UserUI_G";
    public static final String USER_G_FIRST_NAME = "UI_G";
    public static final String USER_G_LAST_NAME = "UserUI_G";
    public static final String USER_G_EMAIL = "UserUI_G@abv.bg";
    public static final String USER_G_PUBLIC_POST = "Public post of UserUI_G";
    public static final String USER_G_PRIVATE_POST = "Private post of UserUI_G";
    public static final String USER_G_BUDDIES_ONLY_POST = "Buddies only post of UserUI_G";
}