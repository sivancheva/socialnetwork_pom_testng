package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends BasePage {

    //Web elements
    @FindBy(xpath = "//a[@title='My Account']")
    private WebElement usernameRight;
    @FindBy(xpath = "//input[@name='search']")
    private WebElement searchField;
    @FindBy(xpath = "//button[@type='submit']")
    private WebElement searchIcon;
    @FindBy(xpath = "//a[@title='Logout']//i")
    private WebElement logOutButton;
    @FindBy(xpath = "//*[@alt='Profile Picture']")
    private WebElement pictureAfterSearch;
    @FindBy(xpath = "//button[@title = 'Sent Request']")
    private WebElement addBuddyButton;
    @FindBy(xpath = "//button[@title = 'Remove from Buddies']")
    private WebElement removeBuddyBtn;


    //Constructor
    public MainPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    //Assert Page is Loaded
    public boolean mainPageIsLoaded() {
        return pageIsLoaded(usernameRight);
    }

    //Page Methods
    public UserPage goToLoggedUsersPage() {
        clickElement(usernameRight);
        return new UserPage(driver);
    }

    public void searchForUser(String name){
        clickElement(searchField);
        writeText(searchField, name);
        clickElement(searchIcon);
    }

    public void sendBuddyRequest(String name){
        searchForUser(name);
        clickElement(addBuddyButton);
    }

    public UserPage goToSearchedUsersPage(String name){
        searchForUser(name);
        clickElement(pictureAfterSearch);
        return new UserPage(driver);
    }

    public void logOut(){
        clickElement(logOutButton);
    }

    public LoginPage goToLoginPage(){
        logOut();
        return  new LoginPage(driver);
    }
}
