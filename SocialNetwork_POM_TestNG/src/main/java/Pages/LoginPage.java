package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

//social-network -ready
public class LoginPage extends BasePage {

    @FindBy(xpath = "//h2[text()='Login Form']")
    private WebElement header;
    @FindBy(xpath = "//input[@id='username']")
    private WebElement usernameInput;
    @FindBy(xpath = "//input[@id='password']")
    private WebElement passwordInput;
    @FindBy(xpath = "//button[@id='loginbutton']")
    private WebElement loginBtn;
    @FindBy(xpath = "//*[@class='error']")
    private WebElement wrongPasswordMessage;
    @FindBy(xpath = "//a[text()='Sign up']")
    private WebElement signUpLink;
    @FindBy(xpath = "//*[@class='error']")
    private WebElement getWrongPasswordMessage;
    @FindBy(xpath = "//h2[text()='Login Form']")
    private WebElement loginHeader;

    //constructor
    LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    //Assert
    public boolean loginPageIsLoaded() {
        return pageIsLoaded(loginHeader);
    }

    public boolean errorMessageWrongUsernameOrPasswordIsVisible(){
        return elementIsVisible(wrongPasswordMessage);
    }

    //Page Methods

    private void clickLoginButton() {
        clickElement(loginBtn);
    }

    private void enterUsername(String username) {
        waitClickable(usernameInput);
        usernameInput.clear();
        usernameInput.sendKeys(username);
    }

    private void enterPassword(String pass) {
        waitClickable(passwordInput);
        passwordInput.clear();
        passwordInput.sendKeys(pass);
    }

    public MainPage logIn(String username, String pass) {
        enterUsername(username);
        enterPassword(pass);
        loginBtn.click();
        return new MainPage(driver);
    }

    public String getLoginErrorMessage(){
        return getText(wrongPasswordMessage);
    }

    public void gotoHomePage(WebDriver driver){
       driver.navigate().back();
    }

    public RegisterPage goToRegisterPage(){
        clickElement(signUpLink);
        return new RegisterPage(driver);
    }
}
