package Pages;

import Constants.Constants;
import Logger.ProjectLogger;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {
    public WebDriver driver;
    private WebDriverWait wait;

    //Constructor
    BasePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    //Wait Wrapper Method
    void waitVisibility(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
        ProjectLogger.LOG.info("Waiting for visibility of: " + element);
    }

    void waitClickable(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        ProjectLogger.LOG.info("Waiting to be clickable: " + element);
    }

    //Read Text
    String getText(WebElement element) {
        ProjectLogger.LOG.info("Getting text of: " + element);
        waitVisibility(element);
        return element.getText();
    }

    public String getPageTitle() {
        ProjectLogger.LOG.info("Getting page title.");
        return driver.getTitle();
    }

    //Click Method
    public void clickElement(WebElement element) {
        ProjectLogger.LOG.info("Clicking on  " + element);
        waitVisibility(element);
        element.click();
    }

    //Write Text - SendKeys
    public void writeText(WebElement element, String text) {
        ProjectLogger.LOG.info("Writing " + text + " in " + element);
        waitVisibility(element);
        clearText(element);
        element.sendKeys(text);
    }

    public boolean pageIsLoaded(WebElement element) {
        ProjectLogger.LOG.info("Performing \"PageIsLoaded()\" Method");
        element = wait.until(ExpectedConditions.visibilityOf(element));
        return element.isDisplayed();
    }

    ;

    public void clearText(WebElement element) {
        element = wait.until(ExpectedConditions.visibilityOf(element));
        ProjectLogger.LOG.info("Clearing text in " + element);
        element.clear();
    }

    public boolean elementIsVisible(WebElement element) {
        element = wait.until(ExpectedConditions.visibilityOf(element));
        ProjectLogger.LOG.info("Asserting visibility of " + element);
        return element.isDisplayed();
    }

    public void goHomePage() {
        ProjectLogger.LOG.info("Performing \"goHomePage()\" method");
        driver.get(Constants.BASE_URL);
    }

    private static boolean isDialogPresent(WebDriver driver) {
        try {
            driver.getTitle();
            return false;
        } catch (UnhandledAlertException e) {
            return true;
        }
    }

    public boolean isAlertPresent()
    {
        try
        {
            driver.switchTo().alert();
            return true;
        }   // try
        catch (NoAlertPresentException Ex)
        {
            return false;
        }   // catch
    }   // isAlertPresent()


}
