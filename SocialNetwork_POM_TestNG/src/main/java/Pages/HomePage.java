package Pages;

import Logger.ProjectLogger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

//social-network -ready
public class HomePage extends BasePage {

    @FindBy(xpath = "//a[@title='Login']")
    private WebElement loginBtn;

    @FindBy(xpath = "//a[@title='Register']")
    private WebElement signUpBtn;

    @FindBy(xpath = "//input[@name='search']")
    private WebElement searchBar;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement searchIcon;

    @FindBy(xpath = "//a[@title='Logout']/i")
    private WebElement logoutBtn;


    //Constructor
    public HomePage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    //Assert
    public boolean homePageIsLoaded() {
        return pageIsLoaded(loginBtn);
    }

    //Page Methods
    public LoginPage gotoLoginPage() {
        ProjectLogger.LOG.info("Performing \"gotoLoginPage()\" method");
        clickElement(loginBtn);
        return new LoginPage(driver);
    }

    public RegisterPage gotoRegisterPage() {
        ProjectLogger.LOG.info("Performing \"gotoRegisterPage()\" method");
        clickElement(signUpBtn);
        return new RegisterPage(driver);
    }
}
