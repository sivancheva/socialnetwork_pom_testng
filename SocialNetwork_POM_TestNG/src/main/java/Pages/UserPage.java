package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserPage extends BasePage {

    //Webelements
    @FindBy(xpath = "//a[text()='Home']")
    private WebElement homeButton;

    @FindBy(xpath = "//i[@class='fa fa-user']")
    private WebElement userIcon;

    //?? should I delete??
    @FindBy(xpath = "//input[@name='search']")
    private WebElement searchField;

    //?? should I delete??
    @FindBy(xpath = "//button[@type='submit']")
    private WebElement searchIcon;





    //constructor
    UserPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
}
