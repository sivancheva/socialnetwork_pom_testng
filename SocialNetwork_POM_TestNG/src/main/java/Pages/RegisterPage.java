package Pages;

import Logger.ProjectLogger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

//social-network -ready
public class RegisterPage extends BasePage {

    @FindBy(xpath = "//h1[text() = 'Register']")
    private WebElement headerh1;
    @FindBy(xpath = "//input[@id='username']")
    private WebElement usernameInput;
    @FindBy(xpath = "//input[@id='password']")
    private WebElement passwordInput;
    @FindBy(xpath = "//input[@id='passwordConfirmation']")
    private WebElement repeatPasswordInput;
    @FindBy(xpath = "//button[@id='registerbutton']")
    private WebElement registerBtn;
    @FindBy(xpath = "//h2[text()='Registration Confirmation']")
    private WebElement confirmationHeader;
    @FindBy(xpath = "//a[text()='Login']")
    private WebElement confirmationLink;


    //Constructor
    public RegisterPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    //Assert Register Page is loaded
    public boolean registerPageIsLoaded() {
        return pageIsLoaded(registerBtn);
    }


    private void enterUsername(String username) {
        waitVisibility(usernameInput);
        usernameInput.clear();
        usernameInput.sendKeys(username);
    }

    private void enterPassword(String pass) {
        waitVisibility(passwordInput);
        passwordInput.clear();
        passwordInput.sendKeys(pass);
    }

    private void enterRepeatePassword(String pass) {
        waitVisibility(repeatPasswordInput);
        repeatPasswordInput.clear();
        repeatPasswordInput.sendKeys(pass);
    }

    public void register(String username, String pass)  {
        if (this.isAlertPresent()){
            ProjectLogger.LOG.info("Dismissing alert." );
            driver.switchTo().alert().dismiss();
        }
        enterUsername(username);
        enterPassword(pass);
        enterRepeatePassword(pass);
        registerBtn.click();
    }

    public LoginPage registerSuccessfully(String username, String pass) {
        register(username, pass);
        clickElement(confirmationLink);
        return new LoginPage(driver);
    }

    public MainPage gotoMainPage() {
        clickElement(registerBtn);
        return new MainPage(driver);
    }

    public void reloadRegisterPage(){
        driver.navigate().refresh();
    }
}
