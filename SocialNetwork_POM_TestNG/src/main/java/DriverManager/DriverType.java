package DriverManager;

public enum DriverType {
    CHROME, FIREFOX, EDGE, SAFARI
}
