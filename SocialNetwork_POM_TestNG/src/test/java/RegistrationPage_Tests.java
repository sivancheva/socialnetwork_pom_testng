import Constants.Constants;
import Pages.HomePage;
import Pages.LoginPage;
import Pages.RegisterPage;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class RegistrationPage_Tests extends BaseTest {
    private HomePage homePage;
    private RegisterPage registerPage;

    @BeforeClass
    public void setUp() {
        homePage = new HomePage(driver);
        registerPage = homePage.gotoRegisterPage();
    }

    @Test(groups = {"register", "positive"} )
    public void TC01_RegisterWithValidCredentials() {
        LoginPage loginPage = registerPage.registerSuccessfully(RandomStringUtils.randomAlphabetic(10), Constants.ALL_USERS_VALID_PASSWORD);
        Assert.assertTrue(loginPage.loginPageIsLoaded());
        loginPage.goToRegisterPage();
    }

    @Test(groups = {"register", "positive"} )
    public void TC02_RegisterWithValidUsernameMinSymbols() {
        LoginPage loginPage = registerPage.registerSuccessfully(RandomStringUtils.randomAlphabetic(5), Constants.ALL_USERS_VALID_PASSWORD);
        Assert.assertTrue(loginPage.loginPageIsLoaded());
        loginPage.goToRegisterPage();
    }

    @Test(groups = {"register", "positive"} )
    public void TC03_RegisterWithValidUsernameMaxSymbols() {
        LoginPage loginPage = registerPage.registerSuccessfully(RandomStringUtils.randomAlphabetic(30), Constants.ALL_USERS_VALID_PASSWORD);
        Assert.assertTrue(loginPage.loginPageIsLoaded());
        loginPage.goToRegisterPage();
    }

    //Negative tests
    @Test(groups = {"register", "negative"} )
    public void TC04_RegisterWithEmptyCredentials() {
        registerPage.register(Constants.EMPTY_STRING, Constants.EMPTY_STRING);
        Assert.assertTrue(registerPage.registerPageIsLoaded());
        registerPage.reloadRegisterPage();
    }

    @Test(groups = {"register", "negative"} )
    public void TC05_RegisterWithShorterUsername() {
        registerPage.register(RandomStringUtils.randomAlphabetic(4), Constants.ALL_USERS_VALID_PASSWORD);
        Assert.assertTrue(registerPage.registerPageIsLoaded());
        registerPage.reloadRegisterPage();
    }

    @Test(groups = {"register", "negative"} )
    public void TC06_RegisterWithLongerUsername() {
        registerPage.register(RandomStringUtils.randomAlphabetic(31), Constants.ALL_USERS_VALID_PASSWORD);
        Assert.assertTrue(registerPage.registerPageIsLoaded());
        registerPage.reloadRegisterPage();
    }

    @Test(groups = {"register", "negative"} )
    public void TC07_RegisterWithShorterPassword() {
        registerPage.register(RandomStringUtils.randomAlphabetic(10), RandomStringUtils.randomAlphanumeric(4));
        Assert.assertTrue(registerPage.registerPageIsLoaded());
        registerPage.reloadRegisterPage();
    }

    @Test(groups = {"register", "negative"} )
    public void TC08_RegisterWithLongerPassword() {
        registerPage.register(RandomStringUtils.randomAlphabetic(10), RandomStringUtils.randomAlphanumeric(26));
        Assert.assertTrue(registerPage.registerPageIsLoaded());
        registerPage.reloadRegisterPage();
    }

    @Test(groups = {"register", "negative"} )
    public void TC09_RegisterAlreadyRegisteredUser() {
        registerPage.register(Constants.USER_A_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        Assert.assertTrue(registerPage.registerPageIsLoaded());
        registerPage.reloadRegisterPage();
    }

}
