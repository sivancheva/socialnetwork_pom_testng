import Constants.Constants;
import Pages.HomePage;
import Pages.LoginPage;
import Pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LoginPage_Tests extends BaseTest {
    private LoginPage loginPage;

    @BeforeClass(groups = {"login", "positive", "negative"} )
    public void setUp() {
        HomePage homePage = new HomePage(driver);
        loginPage = homePage.gotoLoginPage();
    }

    @Test(groups = {"login", "positive"} )
    public void logInWithValidCredentials() {
        MainPage mainPage = loginPage.logIn(Constants.USER_A_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        Assert.assertTrue(mainPage.mainPageIsLoaded());
        mainPage.logOut();
    }

    @Test(groups = {"login", "positive"} )
    public void logInWithValidCredentialsLowerCase() {
        MainPage mainPage = loginPage.logIn(Constants.USER_A_USERNAME.toLowerCase(), Constants.ALL_USERS_VALID_PASSWORD);
        Assert.assertTrue(mainPage.mainPageIsLoaded());
        mainPage.logOut();
    }


    @Test(groups = {"login", "negative"} )
    public void logInWithEmptyCredentials() {
        loginPage.logIn(Constants.EMPTY_STRING, Constants.EMPTY_STRING);
        Assert.assertTrue(loginPage.errorMessageWrongUsernameOrPasswordIsVisible());
    }

}
