
import Constants.Constants;
import DriverManager.DriverManager;
import DriverManager.DriverManagerFactory;
import DriverManager.DriverType;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;


public abstract class BaseTest {
    public static final Logger LOG = LogManager.getRootLogger();
    private DriverManager driverManager;
    WebDriver driver;

    @BeforeClass
    public void setup() {
        driverManager = DriverManagerFactory.getDriverManager(DriverType.FIREFOX);
        driver = driverManager.getWebDriver();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.get(Constants.BASE_URL);
    }

    @BeforeMethod
    public void nameBefore(Method method) {
        LOG.info("Performing test:" + method.getName());
    }

    @AfterClass
    public void teardown() {
        driverManager.quitWebDriver();
    }

}
